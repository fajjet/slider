import styles from '../styles/Home.module.css';
import Slider from '../components/Slider';
import data from '../components/Slider/data';

export default function Home() {
  return (
    <div className={styles.container}>
      <img src={'/images/shape.svg'} className={styles.background}/>
      <div className={styles.sliderWrapper}>
        <Slider slides={data}/>
      </div>
    </div>
  )
}
