import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Icon from '../Icon';
import styles from './Slider.module.css';

const Slider = function (props) {
  const { slides, autoplay, duration } = props;
  if (slides.length === 0) return null;

  const timer = useRef(null);
  const [activeIndex, setActiveIndex] = useState(0);

  function slideTo(index) {
    setActiveIndex(index);
    if (autoplay) {
      clearTimeout(timer.current);
      autoPlay(index);
    }
  }

  function autoPlay(current) {
    timer.current = setTimeout(() => {
      const nextIndex = ( current + 1 ) % slides.length;
      slideTo(nextIndex);
    }, duration * 1000);
  };

  const onStageClick = (index) => {
    if (index === activeIndex) return;
    slideTo(index);
  };

  useEffect(() => {
    if (autoplay) autoPlay(activeIndex);
  }, []);

  return (
    <div className={styles.container}>
      <ul className={styles.stages}>
        {slides.map((slide, i) => {
          const isActive = activeIndex === i;
          return (
            <li key={slide.title} className={styles.stage} onClick={e => onStageClick(i)}>
              <span className={styles.stageText}>{slide.title}</span>
              <div className={classnames(styles.stageSwitch, { [styles.stageSwitchActive]: isActive })}>
                <svg viewBox="0 0 100 100"
                     xmlns="http://www.w3.org/2000/svg"
                     className={styles.stageSwitchSvg}
                >
                  <circle cx="50" cy="50" r="48"/>
                  <circle cx="50" cy="50" r="48"/>
                </svg>
                <div className={styles.stageIcon}><Icon name={slide.icon}/></div>
              </div>
            </li>
          )
        })}
      </ul>
      <div className={styles.slides}>
        {slides.map((slide, i) => {
          const isActive = activeIndex === i;
          return (
            <div key={slide.title} className={styles.slideWrapper}>
              <div className={classnames(styles.slide, { [styles.slideActive]: isActive })}>
                <h2 className={styles.slideTitle}>{slide.title}</h2>
                <p className={styles.slideDescription}>{slide.description}</p>
                <img className={styles.slideImage} src={slide.image}/>
              </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}

Slider.propTypes = {
  duration: PropTypes.number,
  autoplay: PropTypes.bool,
  slides: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    image: PropTypes.string,
    icon: PropTypes.string,
  }))
};

Slider.defaultProps = {
  autoplay: true,
  duration: 5,
};

export default Slider;
