import React from 'react';
import PropTypes from 'prop-types';

import * as icons from './static';

const iconNames = Object.keys(icons);

const Icon = (props) => {
  const { name, fill, ...restProps } = props;
  const SvgIcon = icons[name];

  return <SvgIcon fill={fill} {...restProps}/>;
};

Icon.propTypes = {
  name: PropTypes.oneOf(iconNames),
}

Icon.defaultProps = {
  fill: 'currentColor',
}

export default Icon;